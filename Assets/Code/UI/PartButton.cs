﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PartButton : MonoBehaviour {
    public GameObject prefab;

    public void OnClick() {
        if(FloorPlacementController.thingToPlace != null) {
            Destroy(FloorPlacementController.thingToPlace.gameObject);
        }
        FloorPlacementController.thingToPlace = Instantiate(prefab).GetComponent<AbstractPlacedItem>();
        Assert.IsTrue(FloorPlacementController.thingToPlace != null);
    }
}
