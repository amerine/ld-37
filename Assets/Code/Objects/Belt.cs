﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Belt : AbstractPlacedItem {
    string art {
        get {
            if(output != null) {
                var destinationDirection = Grid.GetDirectionTo(this, this.output);
                var inputCount = 0;
                foreach(var input in inputs) {
                    inputCount++;
                    if(inputCount > 1) {
                        break;
                    }
                }

                if(inputCount > 1) {
                    return "Merge";
                } else {
                    var inputDirection = destinationDirection;
                    if(inputCount == 1) {
                        foreach(var input in inputs) {
                            inputDirection = Grid.GetDirectionTo(input, this);
                        }
                    }

                    if(inputDirection == destinationDirection) {
                        return "Straight";
                    } else if(inputDirection.GetLeftOf() == destinationDirection
                        || inputDirection.GetRightOf() == destinationDirection) {
                       
                        //if(inputDirection == EnumDirection.North && destinationDirection == EnumDirection.East) {
                        //    transform.rotation = Quaternion.Euler(0, -90, 0);
                        //} else if(inputDirection == EnumDirection.East && destinationDirection == EnumDirection.South) {
                        //    transform.rotation = Quaternion.Euler(0, 0, 0);
                        //} else if(inputDirection == EnumDirection.West && destinationDirection == EnumDirection.North) {
                        //    transform.rotation = Quaternion.Euler(0, 180, 0);
                        //} else {
                        //    transform.rotation = Quaternion.Euler(0, 90, 0);
                        //}
                        return "Curve";
                    } else {
                        Assert.IsTrue(false);
                    }
                }
            } 
            return "Straight";
        }
    }

    private void Update() {
        OnConnectionChange(); // TODO this is needed to update when editing track, but there should be a better way..
    }

    public override void OnConnectionChange() {
        base.OnConnectionChange();
        var shouldShowShort = false;
        if(output != null) {
            if(output is Belt) {
                var outputBelt = (Belt)output;
                if(outputBelt.art == "Merge"
                    && Grid.GetDirectionTo(this, this.output) == outputDirection
                    && outputDirection != outputBelt.outputDirection) {
                    shouldShowShort = true;
                }
            }
        }

        EnumDirection? destinationDirection = null;
        if(this.output != null) {
            destinationDirection = Grid.GetDirectionTo(this, this.output);
        }
        for(int i = 0; i < transform.childCount; i++) {
            var child = transform.GetChild(i);
            if(child.name != "Sphere" && child.name != "Canvas") {
                if(child.name == "Short") {
                    child.gameObject.SetActive(shouldShowShort);
                } else {
                    child.gameObject.SetActive(child.name == art);
                    if(art == "Curve" && child.name == art) {
                        var inputDirection = destinationDirection.Value;
                        foreach(var input in inputs) {
                            inputDirection = Grid.GetDirectionTo(input, this);
                        }
                        if(inputDirection.GetLeftOf() == destinationDirection.Value) {
                            child.transform.localRotation = Quaternion.Euler(-90, 90, 0);
                        } else {
                            child.transform.localRotation = Quaternion.Euler(-90, 180, 0);
                        }
                    }
                }
            }
        }
    }
}

