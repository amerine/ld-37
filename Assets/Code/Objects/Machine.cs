﻿using System;
using UnityEngine;


public class Machine : AbstractPlacedItem {
    #region Data / Init
    public string inputType;
    public int inputCountPerProduct;
    int productsRecieved;
    public GameObject product;
    int ticksSinceLastProduct, ticksPerProduct = 60;
    Product lastProduct;
    Color productColor;

    protected void Start() {
        Assert.IsTrue(inputType != null || inputCountPerProduct == 0);
        Assert.IsTrue(inputCountPerProduct >= 0);
        productColor = UnityEngine.Random.ColorHSV();
    }
    #endregion

    public void FixedUpdate() {
        if(productsRecieved >= inputCountPerProduct) {
            if(output != null) {
                if(ticksSinceLastProduct == 0) {
                    // check collision before spawning
                    if(lastProduct == null || (lastProduct.transform.position - transform.position).magnitude > Product.productCollisionRadius) {
                        int blockX, blockZ;
                        if (this.GetClosestBlockOffset(output, out blockX, out blockZ)) {
                            var productPosition = transform.position + new Vector3(blockX, 0f, blockZ);
                            lastProduct = Instantiate(this.product, productPosition, Quaternion.identity).GetComponent<Product>();
                            lastProduct.currentHolder = this;
                            Assert.IsTrue(lastProduct != null);
                            lastProduct.GetComponentInChildren<Renderer>().material.color = productColor;
                            products.Add(lastProduct); // add to the queue so we can also detect products backing up during Product.IsBlocked
                        }
                    }
                }
            }
            ticksSinceLastProduct++;
            if(ticksSinceLastProduct >= ticksPerProduct) {
                if(output != null) {
                    ticksSinceLastProduct = 0;
                } else {
                    ticksSinceLastProduct--;
                }
            }
        }
    }

    internal bool Consume(Product product) {
        if(product.typeName == inputType) {
            productsRecieved++;
            return true;
        }
        return false;
    }
}

