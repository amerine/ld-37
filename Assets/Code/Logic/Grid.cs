﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class Grid : MonoBehaviour {
    #region Data
    public const uint gridWidth = 100, gridLength = 100;
    static List<AbstractPlacedItem> denseItems = new List<AbstractPlacedItem>((int) (gridWidth * gridLength));
    static AbstractPlacedItem[,] sparseItems = new AbstractPlacedItem[gridWidth, gridLength];

    public static AbstractPlacedItem GetItem(Vector3 position) {
        int x, z;
        if(GetCoords(position, out x, out z)) {
            return GetItem(x, z);
        }
        return null;
    }

    public static AbstractPlacedItem GetItem(int x, int z) {
        return sparseItems[x, z];
    }

    public static bool GetCoords(Vector3 position, out int x, out int z) {
        x = Mathf.RoundToInt(position.x) + (int)gridWidth / 2;
        z = Mathf.RoundToInt(position.z) + (int)gridLength / 2;
        if(x < 0 || x > gridWidth || z < 0 || z > gridLength) {
            MonoBehaviour.print("Out of bounds");
            return false; // out of bounds
        }
        return true;
    }
    #endregion

    #region Public API
    public static IEnumerable<Type> GetSurrounding<Type>(Vector3 position, bool includeCenterSquare = false)
        where Type : AbstractPlacedItem
    {
        int x, z;
        var surroundings = new HashSet<Type>();

        if (GetCoords(position, out x, out z)) {
            var item = GetItem(x, z);
            if (item != null) {
                surroundings.UnionWith(GetSurrounding<Type>(item, includeCenterSquare));
            }
        }

        return surroundings;
    }

    public static IEnumerable<Type> GetSurrounding<Type>(AbstractPlacedItem item, bool includeCenterSquare = false)
        where Type : AbstractPlacedItem
    {
        var surroundings = new HashSet<Type>();

        int x, z;
        if (GetCoords(item.transform.position, out x, out z)) {
            if (includeCenterSquare && GetItem(x, z) == item && item is Type) {
                surroundings.Add((Type) item);
            }

            var blocks = item.GetRotatedBlocks();
            for (int iBlock = 0; iBlock < blocks.Length; ++iBlock) {
                int blockX = x + Mathf.RoundToInt(blocks[iBlock].x);
                int blockZ = z + Mathf.RoundToInt(blocks[iBlock].z);

                for (int checkX = blockX - 1; checkX <= blockX + 1; checkX++) {
                    for (int checkZ = blockZ - 1; checkZ <= blockZ + 1; checkZ++) {
                        if (checkX != blockX ^ checkZ != blockZ) {
                            var checkItem = GetItem(checkX, checkZ);
                            if (checkItem != null && checkItem != item && checkItem is Type) {
                                surroundings.Add((Type) checkItem);
                            }
                        }
                    }
                }
            }
        }

        return surroundings;
    }

    public static IEnumerable<Type> GetAbstractPlacedItems<Type>() where Type : AbstractPlacedItem {
        foreach (var item in denseItems) {
            var castItem = item as Type;
            if (castItem != null) {
                yield return castItem;
            }
        }
    }   

    public static bool PlaceItem(Vector3 position, AbstractPlacedItem itemToPlace) {
        int x, z;
        if(GetCoords(position, out x, out z)) {
            return PlaceItem(x, z, itemToPlace);
        }
        return false;
    }

    public static bool PlaceItem (int x, int z, AbstractPlacedItem itemToPlace)
    {
        var blocks = itemToPlace.GetRotatedBlocks();
        // first, check if all slots are empty
        for (int i = 0; i < blocks.Length; ++i) {
            int blockX = x + Mathf.RoundToInt(blocks[i].x);
            int blockZ = z + Mathf.RoundToInt(blocks[i].z);
            if (sparseItems[blockX, blockZ] != null) {
                Assert.IsTrue(false);
                return false;
            }
        }
        // then set them to this item, if we are still here.
        for (int i = 0; i < blocks.Length; ++i) {
            int blockX = x + Mathf.RoundToInt(blocks[i].x);
            int blockZ = z + Mathf.RoundToInt(blocks[i].z);
            sparseItems[blockX, blockZ] = itemToPlace;
        }

        MonoBehaviour.print("Grid added " + itemToPlace + " @ " + x + ", " + z + " + " + String.Join("; ", System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Select(blocks, b => b.ToString()))));

        denseItems.Add(itemToPlace);
        Network.UpdateGrid(itemToPlace);
        return true;
    }


    public static bool RemoveItem(AbstractPlacedItem itemToRemove) {
        int x, z;
        if (GetCoords(itemToRemove.transform.position, out x, out z)) {
            var blocks = itemToRemove.GetRotatedBlocks();
            // first, check if all slots are actually belonging to that item,
            for (int i = 0; i < blocks.Length; ++i) {
                int blockX = x + Mathf.RoundToInt(blocks[i].x);
                int blockZ = z + Mathf.RoundToInt(blocks[i].z);
                if (sparseItems[blockX, blockZ] != itemToRemove) {
                    Assert.IsTrue(false);
                    return false;

                }
            }

            // then clear them, if we are still here.
            itemToRemove.RemoveFromWorld();

            for (int i = 0; i < blocks.Length; ++i) {
                int blockX = x + Mathf.RoundToInt(blocks[i].x);
                int blockZ = z + Mathf.RoundToInt(blocks[i].z);
                sparseItems[blockX, blockZ] = null;
            }

            Debug.Assert(denseItems.Remove(itemToRemove));

            Network.UpdateGrid(itemToRemove);
            // MonoBehaviour.print("Grid added " + itemToPlace + " @ " + x + ", " + z);
            return true;
        }
        return false;
    }

    public static EnumDirection GetDirectionTo(AbstractPlacedItem origin, AbstractPlacedItem destination) {
        if(Mathf.RoundToInt(origin.transform.position.x) ==
            Mathf.RoundToInt(destination.transform.position.x)) {
            if(origin.transform.position.z < destination.transform.position.z) {
                return EnumDirection.North;
            } else {
                return EnumDirection.South;
            }
        } else {
            if(origin.transform.position.x < destination.transform.position.x) {
                return EnumDirection.East;
            } else {
                return EnumDirection.West;
            }
        }
    }

    // this is the new version of the function above.
    // the GetDirectionTo function is no longer used.
    // instead of just returning the primary direction of 2 blocks,
    // this checks if ANY 2 blocks of both AbstractPlacedItem's are 
    // relative to each other in a certain direction.
    public static bool CheckDirectionTo (AbstractPlacedItem origin, AbstractPlacedItem destination, EnumDirection direction)
    {
        var srcBlocks = origin.GetRotatedBlocks();
        var destBlocks = destination.GetRotatedBlocks();
        for (int iSrc = 0; iSrc < srcBlocks.Length; ++iSrc) {
            for (int iDest = 0; iDest < destBlocks.Length; ++iDest) {
                int x0 = Mathf.RoundToInt(origin.transform.position.x + srcBlocks[iSrc].x);
                int z0 = Mathf.RoundToInt(origin.transform.position.z + srcBlocks[iSrc].z);
                int x1 = Mathf.RoundToInt(destination.transform.position.x + destBlocks[iDest].x);
                int z1 = Mathf.RoundToInt(destination.transform.position.z + destBlocks[iDest].z);
                if (Mathf.Abs(x0 - x1) + Mathf.Abs(z0 - z1) <= 1) {
                    if (x0 == x1) {
                        if ((z0 < z1 && direction == EnumDirection.North) || (z0 > z1 && direction == EnumDirection.South)) {
                            return true;
                        }
                    } else {
                        if ((x0 < x1 && direction == EnumDirection.East) || (x0 > x1 && direction == EnumDirection.West)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    #endregion
}

