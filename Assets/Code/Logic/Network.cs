﻿using System;
using UnityEngine;
using System.Collections.Generic;


public static class Network {
    public static void FollowSurroundingBelts(AbstractPlacedItem source) {
        if(source.output == null) {
            foreach(var surroundingPlacedItem in Grid.GetSurrounding<AbstractPlacedItem>(source.transform.position)) {
                if(CanConnectTo(source, surroundingPlacedItem)) {
                    source.output = surroundingPlacedItem;
                    return;
                    //connection.belts.Add(surroundingPlacedItems);
                    //foreach(var connectedMachine in Grid.GetSurrounding<Machine>(surroundingPlacedItems.transform.position)) {
                    //    if(connection.CanConnectTo(connectedMachine)
                    //        && connectedMachine != connection.from
                    //        && connection.from.outputConnection == null) {
                    //        // Connection found
                    //        MonoBehaviour.print("Connection found");
                    //        connection.to = connectedMachine;
                    //        connection.from.outputConnection = connection.Clone();
                    //        connection.from.outputConnection.from.connection = connection.from.outputConnection;
                    //        connection.from.outputConnection.to.connection = connection.from.outputConnection;
                    //        for(int i = 0; i < connection.from.outputConnection.belts.Count; i++) {
                    //            connection.from.outputConnection.belts[i].connection = connection.from.outputConnection;
                    //        }
                    //    }
                    //}
                    //FollowSurroundingBelts(surroundingPlacedItems.transform.position);
                    //connection.belts.Remove(surroundingPlacedItems);
                }
            }
        }
    }

    static bool CanConnectTo(AbstractPlacedItem source, AbstractPlacedItem destination) {
        var canConnectToOutputDirection = Grid.CheckDirectionTo(source, destination, source.outputDirection);
        if (source.outputDirection == destination.inputDirection
            && canConnectToOutputDirection) {
            return true; // straight line
        } else if(source.outputDirection.GetLeftOf() == destination.inputDirection
            && (Grid.CheckDirectionTo(source, destination, source.outputDirection.GetLeftOf()) // left turn
                || canConnectToOutputDirection)) { // side connection
            return true;
        } else if(source.outputDirection.GetRightOf() == destination.inputDirection
            && (Grid.CheckDirectionTo(source, destination, source.outputDirection.GetRightOf()) // right turn 
                || canConnectToOutputDirection)) { // side connection
            return true;
        } else {
            return false;
        }
    }

    internal static void UpdateGrid (AbstractPlacedItem item)
    {
        foreach (var surrounding in Grid.GetSurrounding<AbstractPlacedItem>(item, true)) {
            FollowSurroundingBelts(surrounding);
        }
    }
}
