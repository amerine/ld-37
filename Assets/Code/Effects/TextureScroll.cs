﻿using UnityEngine;

public class TextureScroll : MonoBehaviour {
    public Vector2 speed;
    public string textureName = "_MainTex";

    private Renderer rend;

    void Start() {
        rend = GetComponent<Renderer>();
    }

    public void Update() {
        var currentOffs = rend.material.GetTextureOffset(textureName);
        currentOffs += speed * Time.deltaTime;
        rend.material.SetTextureOffset(textureName, currentOffs);
    }
}