﻿public enum EnumDirection {
    North, East, South, West
}

public static class EnumDirectionExtensions {
    public static EnumDirection GetLeftOf(this EnumDirection from) {
        switch(from) {
        case EnumDirection.North:
        default:
            return EnumDirection.West;
        case EnumDirection.East:
            return EnumDirection.North;
        case EnumDirection.South:
            return EnumDirection.East;
        case EnumDirection.West:
            return EnumDirection.South;
        }
    }
    public static EnumDirection GetRightOf(this EnumDirection from) {
        switch(from) {
        case EnumDirection.North:
        default:
            return EnumDirection.East;
        case EnumDirection.East:
            return EnumDirection.South;
        case EnumDirection.South:
            return EnumDirection.West;
        case EnumDirection.West:
            return EnumDirection.North;
        }
    }
}

