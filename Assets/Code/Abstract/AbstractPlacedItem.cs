﻿using System;
using UnityEngine;
using System.Collections.Generic;


public abstract class AbstractPlacedItem : MonoBehaviour {
    #region Data / Init
    public List<Product> products;
    AbstractPlacedItem _output;

    public IEnumerable<AbstractPlacedItem> inputs {
        get {
            foreach(var sibling in Grid.GetSurrounding<AbstractPlacedItem>(transform.position)) {
                if(sibling.output == this) {
                    yield return sibling;
                }
            }
        }
    }
    
    public AbstractPlacedItem output {
        set {

            _output = value;
            OnConnectionChange();
        }
        get {
            return _output;
        }
    }
    


    public EnumDirection inputDirection {
        get {
            return outputDirection; // TODO
        }
    }

    public virtual void OnConnectionChange() {
    }

    public virtual EnumDirection outputDirection {
        get {
            var yRotation = transform.rotation.eulerAngles.y;
            switch(Mathf.RoundToInt(yRotation)) {
            case 0:
                return EnumDirection.North;
            case 90:
                return EnumDirection.East;
            case 180:
                return EnumDirection.South;
            case 270:
                return EnumDirection.West;
            default:
                Assert.IsTrue(false);
                return EnumDirection.North;
            }
        }
    }

    public int gridX {
        get {
            int x, y;
            Grid.GetCoords(transform.position, out x, out y);
            return x;
        }
    }
    public int gridZ {
        get {
            int x, y;
            Grid.GetCoords(transform.position, out x, out y);
            return y;
        }
    }

    public virtual Vector3[] blocks
    {
        get {
            return new Vector3[] { new Vector3(0f, 0f, 0f) };
        }
    }
    #endregion

    #region Public API
    public Vector3[] GetRotatedBlocks ()
    {
        var blocks = this.blocks;
        for (int i = 0; i < blocks.Length; ++i) {
            blocks[i] = transform.rotation * blocks[i];
        }
        return blocks;
    }

    public bool GetClosestBlockOffset (AbstractPlacedItem item, out int x, out int z)
    {
        x = 0;
        z = 0;

        int ownX, ownZ, itemX, itemZ;
        if (Grid.GetCoords(transform.position, out ownX, out ownZ) && Grid.GetCoords(item.transform.position, out itemX, out itemZ)) {
            var ownBlocks = GetRotatedBlocks();
            var itemBlocks = item.GetRotatedBlocks();

            int smallestDistance = int.MaxValue;
            for (int iOwn = 0; iOwn < ownBlocks.Length; ++iOwn) {
                int ownBlockX = ownX + Mathf.RoundToInt(ownBlocks[iOwn].x);
                int ownBlockZ = ownZ + Mathf.RoundToInt(ownBlocks[iOwn].z);

                for (int iItem = 0; iItem < itemBlocks.Length; ++iItem) {
                    int itemBlockX = itemX + Mathf.RoundToInt(itemBlocks[iItem].x);
                    int itemBlockZ = itemZ + Mathf.RoundToInt(itemBlocks[iItem].z);
                    // using manhattan distance here.
                    int distance = Mathf.Abs(ownBlockX - itemBlockX) + Mathf.Abs(ownBlockZ - itemBlockZ);
                    if (distance < smallestDistance) {
                        x = Mathf.RoundToInt(ownBlocks[iOwn].x);
                        z = Mathf.RoundToInt(ownBlocks[iOwn].z);
                        smallestDistance = distance;
                    }
                }
            }

            return (smallestDistance < int.MaxValue);
        }
        return false;
    }
    #endregion

    #region Event
    public void OnClick() {
        Grid.RemoveItem(this);
        FloorPlacementController.thingToPlace = this;
    }

    internal void RemoveFromWorld() {
        for(int i = 0; i < products.Count; i++) {
            Destroy(products[i].gameObject);
        }
        products.Clear();
        output = null;
    }
    #endregion
}

